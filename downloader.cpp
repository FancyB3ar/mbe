#include "downloader.h"

Downloader::Downloader(QObject *parent) : QObject(parent)
{
    pnam = new QNetworkAccessManager(this);
    connect(pnam, SIGNAL(finished(QNetworkReply*)), SLOT(slotFinished(QNetworkReply*)));
}

void Downloader::download(const QUrl& url)
{
    QByteArray qba_data;
    qba_data.append("param1=1&param2=2");
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QNetworkReply* pnr = pnam->post(request, qba_data);
    Q_UNUSED(pnr);
}

void Downloader::slotFinished(QNetworkReply* pnr)
{
    if(pnr->error() != QNetworkReply::NoError)
    {
        emit signalError(pnr->errorString());
    }
    else
    {
        QString strAnswer = QString::fromUtf8(pnr->readAll());
        emit signalDone(strAnswer);
    }

    pnr->deleteLater();
}
