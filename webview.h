#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QtWidgets>
#include <QWebEngineView>
#include <QWebEngineHistory>
#include <QNetworkInterface>

#include "downloader.h"

class WebView : public QWidget
{
    Q_OBJECT

private:
    const QString strNameBrowser = "My Browser";

    QWebEngineView* wev_mobileBrowser;
    QPushButton* pb_back;
    QPushButton* pb_forward;
    QPushButton* pb_refresh_stop;
    QPushButton* pb_home;
    QString strHome;
    QLineEdit* le_address;
    QPushButton* pb_go;
    QProgressBar* pbar_load;

    QCheckBox* cb_network;
    QString strNameNetInterface;

    Downloader* downloader;
    QPushButton* pb_postRequest;
    QString strDownloadLink;

    QHBoxLayout* hbl_1;
    QHBoxLayout* hbl_2;
    QVBoxLayout* vbl_1;

    QString getNetworkInterface();

public:
    WebView(QWidget *parent = nullptr);
    ~WebView();

private slots:
    void slotGo();
    void slotGoHome();
    void slotFinished(bool);
    void slotTextEdited(const QString &);
    void slotEditingFinished();

    void slotChangeNetStatus(int);

    void slotSendPostRequest();
    void slotErrorPostRequest(const QString);
    void slotDonePostRequest(const QString);
};
#endif // WEBVIEW_H
