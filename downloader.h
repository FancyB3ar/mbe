#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class Downloader : public QObject
{
    Q_OBJECT

private:
    QNetworkAccessManager* pnam;

public:
    explicit Downloader(QObject *parent = nullptr);

    void download(const QUrl&);

signals:
    void signalDone(const QString);
    void signalError(const QString);

private slots:
    void slotFinished(QNetworkReply*);
};

#endif // DOWNLOADER_H
