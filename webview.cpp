#include "webview.h"

WebView::WebView(QWidget *parent) : QWidget(parent)
{
    wev_mobileBrowser = new QWebEngineView;
    wev_mobileBrowser->setFixedSize(360, 640);

    pb_back = new QPushButton("←");
    pb_back->setFixedSize(25, 25);
    pb_back->setEnabled(false);
    pb_forward = new QPushButton("→");
    pb_forward->setFixedSize(25, 25);
    pb_forward->setEnabled(false);
    pb_refresh_stop = new QPushButton("⟳");
    pb_refresh_stop->setFixedSize(25, 25);
    pb_home = new QPushButton("⟰");
    pb_home->setFixedSize(25, 25);
    strHome = "http://skydns.ru/home";
    le_address = new QLineEdit("");
    pb_go = new QPushButton("→");
    pb_go->setFixedSize(25, 25);
    pb_go->hide();
    pbar_load = new QProgressBar;
    pbar_load->hide();

    cb_network = new QCheckBox("&Network status");
    cb_network->setChecked(true);

    downloader = new Downloader(this);
    pb_postRequest = new QPushButton("&Send");
    pb_postRequest->setFixedSize(50, 25);
    strDownloadLink = "http://127.0.0.1:8000/api";

    // Настройка расположения объектов
    hbl_1 = new QHBoxLayout;
    hbl_1->addWidget(pb_back);
    hbl_1->addWidget(pb_forward);
    hbl_1->addWidget(pb_refresh_stop);
    hbl_1->addWidget(pb_home);
    hbl_1->addWidget(le_address);
    hbl_1->addWidget(pb_go);

    hbl_2 = new QHBoxLayout;
    hbl_2->addWidget(pbar_load);
    hbl_2->addWidget(cb_network);
    hbl_2->addWidget(pb_postRequest);

    vbl_1 = new QVBoxLayout;
    vbl_1->addLayout(hbl_1);
    vbl_1->addWidget(wev_mobileBrowser);
    vbl_1->addLayout(hbl_2);

    // connects
    connect(pb_go, SIGNAL(pressed()), SLOT(slotGo()));
    connect(le_address, SIGNAL(returnPressed()), SLOT(slotGo()));
    connect(wev_mobileBrowser, SIGNAL(loadFinished(bool)), SLOT(slotFinished(bool)));
    connect(pb_home, SIGNAL(clicked()), SLOT(slotGoHome()));
    connect(pb_back, SIGNAL(clicked()), wev_mobileBrowser, SLOT(back()));
    connect(pb_forward, SIGNAL(clicked()), wev_mobileBrowser, SLOT(forward()));
    connect(wev_mobileBrowser, SIGNAL(loadProgress(int)), pbar_load, SLOT(setValue(int)));
    connect(le_address, SIGNAL(textEdited(const QString &)), SLOT(slotTextEdited(const QString &)));
    connect(le_address, SIGNAL(editingFinished()), SLOT(slotEditingFinished()));

    connect(cb_network, SIGNAL(stateChanged(int)), SLOT(slotChangeNetStatus(int)));

    connect(pb_postRequest, SIGNAL(clicked()), SLOT(slotSendPostRequest()));
    connect(downloader, SIGNAL(signalDone(const QString)), SLOT(slotDonePostRequest(const QString)));
    connect(downloader, SIGNAL(signalError(const QString)), SLOT(slotErrorPostRequest(const QString)));

    // Настрока отображения окна
    setWindowTitle("My Browser");
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    setLayout(vbl_1);

    strNameNetInterface = getNetworkInterface();

    slotGoHome();
}

// slotGo ...
void WebView::slotGo()
{
    // Когда адресная строка пуста не реагируем
    if(le_address->text().isEmpty()) return;

    if(!le_address->text().startsWith("http://")
       && !le_address->text().startsWith("https://")
      )
    {
        le_address->setText("http://" + le_address->text());
    }

    // Отображаем полосу загрузки страницы
    pbar_load->show();

    disconnect(pb_refresh_stop, SIGNAL(clicked()), wev_mobileBrowser, SLOT(reload()));
    pb_refresh_stop->setText("X");
    connect(pb_refresh_stop, SIGNAL(clicked()), wev_mobileBrowser, SLOT(stop()));

    wev_mobileBrowser->load(QUrl(le_address->text()));
}

// slotGoHome ...
void WebView::slotGoHome()
{
    le_address->setText(strHome);
    slotGo();
}

// slotFinished ...
void WebView::slotFinished(bool ok)
{
    Q_UNUSED(ok);

    // Меняем название окна и адрес
    setWindowTitle(strNameBrowser + " | " + wev_mobileBrowser->title());
    setWindowIcon(wev_mobileBrowser->icon());
    le_address->setText(wev_mobileBrowser->url().toString());

    // Скрываем полосу загрузки страницы
    pbar_load->hide();

    disconnect(pb_refresh_stop, SIGNAL(clicked()), wev_mobileBrowser, SLOT(stop()));
    pb_refresh_stop->setText("⟳");
    connect(pb_refresh_stop, SIGNAL(clicked()), wev_mobileBrowser, SLOT(reload()));

    pb_back->setEnabled(wev_mobileBrowser->page()->history()->canGoBack());
    pb_forward->setEnabled(wev_mobileBrowser->page()->history()->canGoForward());
}

// slotEditingFinished ...
void WebView::slotEditingFinished()
{
    if(!pb_go->isHidden())
    {
        pb_go->hide();
    }
}

// slotTextEdited ...
void WebView::slotTextEdited(const QString &txt)
{
    if(pb_go->isHidden() && !txt.isEmpty())
    {
        pb_go->show();
    }
    else
    {
        if(txt.isEmpty()) pb_go->hide();
    }
}

// getNetworkInterfase ...
QString WebView::getNetworkInterface()
{
    QList<QNetworkInterface> ifaces = QNetworkInterface::allInterfaces();

    if(!ifaces.isEmpty())
    {
        for(int i = 0; i < ifaces.size(); ++i)
        {
            unsigned int flags = ifaces[i].flags();
            bool isLoopback = (bool)(flags & QNetworkInterface::IsLoopBack);
            bool isP2P = (bool)(flags & QNetworkInterface::IsPointToPoint);
            bool isRunning = (bool)(flags & QNetworkInterface::IsRunning);

            // Если интерфейс не запущен, пропускаем
            if(!isRunning) continue;

            // Если интерфейс невалидный или виртуальный или точка - точка, пропускаем
            if( !ifaces[i].isValid() || isLoopback || isP2P ) continue;

            return ifaces[i].name();
        }
    }

    return "";
}

// slotChangeNetStatus ...
void WebView::slotChangeNetStatus(int status)
{
    QProcess process;

    // Статус отключён
    if(status == Qt::Unchecked)
    {
        process.start("ifconfig " + strNameNetInterface + " down");
//        qDebug() << "ifconfig " + strNameNetInterface + " down";
        process.waitForFinished();
    }
    else
        if(status == Qt::Checked) // Статус включён
        {
            process.start("ifconfig " + strNameNetInterface + " up");
//            qDebug() << "ifconfig " + strNameNetInterface + " up";
            process.waitForFinished();
        }
}

// slotSendPostRequest ...
void WebView::slotSendPostRequest()
{
    downloader->download(QUrl(strDownloadLink));
}

// slotDonePostRequest ...
void WebView::slotDonePostRequest(const QString answer)
{
    QMessageBox m;
    m.setWindowTitle("Answer server");
    m.setText(answer);
    m.setDefaultButton(QMessageBox::Ok);
    m.setIcon(QMessageBox::Information);
    QTimer::singleShot(5000, &m, SLOT(close()));
    m.exec();
}

// slotErrorPostRequest ...
void WebView::slotErrorPostRequest(const QString err)
{
    QMessageBox m;
    m.setWindowTitle("Error");
    m.setText(err);
    m.setDefaultButton(QMessageBox::Ok);
    m.setIcon(QMessageBox::Critical);
    QTimer::singleShot(5000, &m, SLOT(close()));
    m.exec();
}

// ~WebView ...
WebView::~WebView()
{
    // Восстановление рабочего состояния сетевого интерфейса
    QProcess process;
    process.start("ifconfig " + strNameNetInterface + " up");
//    qDebug() << "ifconfig " + strNameNetInterface + " up";
    process.waitForFinished();
}
